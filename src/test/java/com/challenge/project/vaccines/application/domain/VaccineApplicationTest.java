package com.challenge.project.vaccines.application.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class VaccineApplicationTest {

    @Test
    @DisplayName("Register vaccine application success")
    public void registerVaccineApplication(){

        final User user = new UserMock().toUser("035.988.430-03", "joao@dominio.com");

        final VaccinesApplication vaccinesApplication = new VaccineApplicationMock().toVaccineApplication("joao@dominio.com");

        final VaccinesApplication vaccinesReturned = vaccinesApplication.registerVaccinesApplication(
                newVaccinesApplication(),
                newUserOperations( user )
        );

        Assertions.assertEquals( vaccinesApplication, vaccinesReturned);

    }

    @Test
    @DisplayName("Register vaccine application error, user email not registered")
    public void registerVaccineApplicationNotFound(){

        final VaccinesApplication vaccinesApplication = new VaccineApplicationMock().toVaccineApplication("joao.silva@dominio.com");

        Assertions.assertThrows( UserException.class, () -> vaccinesApplication.registerVaccinesApplication(
                newVaccinesApplication(),
                newUserOperations( null )
        ));

    }

    private VaccinesApplicationOperations newVaccinesApplication(){
        return new VaccinesApplicationOperations() {
            @Override
            public VaccinesApplication registerVaccinesApplication(final VaccinesApplication vaccinesApplication) {
                return vaccinesApplication;
            }
        };
    }

    private UserOperations newUserOperations( final User userParameter ){
        return new UserOperationsTest().newUserOperations(userParameter);
    }

}

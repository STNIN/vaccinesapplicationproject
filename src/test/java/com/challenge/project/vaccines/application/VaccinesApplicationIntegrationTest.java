package com.challenge.project.vaccines.application;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@ActiveProfiles( value = "test" )
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext( classMode = DirtiesContext.ClassMode.BEFORE_CLASS )
public class VaccinesApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Register vaccines application success")
    public void registerVaccinesApplication() throws Exception {

        final String requestUser = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-registered.json")
                        .toPath()
        );

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requestUser ));

        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/register-vaccines-application-success.json")
                        .toPath()
        );

        mockMvc.perform(MockMvcRequestBuilders
                .post("/vaccines-application")
                .contentType( MediaType.APPLICATION_JSON )
                .content( request ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isCreated() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE) )
                .andExpect( MockMvcResultMatchers.jsonPath("$.id").value(1L) );

    }

    @Test
    @DisplayName("Register vaccines application without user registered")
    public void saveVaccinesApplicationWithoutUser() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/vaccines-application-without-user.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register vaccines application without name")
    public void saveVaccinesApplicationWithoutName() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/vaccines-application-without-name.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register vaccines application without email")
    public void saveVaccinesApplicationWithoutEmail() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/vaccines-application-without-email.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register vaccines application with invalid email")
    public void saveVaccinesApplicationWithInvalidEmail() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/vaccines-application-email-invalid.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register vaccines application without application date")
    public void saveVaccinesApplicationWithoutApplicationDate() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/vaccines-application-without-application-date.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

}

package com.challenge.project.vaccines.application;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@ActiveProfiles( value = "test" )
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext( classMode = DirtiesContext.ClassMode.BEFORE_CLASS )
public class UserIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Register user success")
    public void registerUser() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content(
                        Files.readString(
                                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/register-user-success.json")
                                        .toPath()
                        )
                ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isCreated() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE) )
                .andExpect( MockMvcResultMatchers.jsonPath("$.id").value(2L) );

    }

    @Test
    @DisplayName("Register user already registered")
    public void saveUserRegisteredTest() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-registered.json")
                        .toPath()
        );
        registerUser( request );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register user email already registered")
    public void saveUserRegisteredEmailTest() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-registered-email.json")
                        .toPath()
        );
        registerUser( request );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Register user cpf already registered")
    public void saveUserRegisteredCpfTest() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-registered-cpf.json")
                        .toPath()
        );
        registerUser( request );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user without name")
    public void trySaveUserWithoutName() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-without-name.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user without cpf")
    public void trySaveUserWithoutCpf() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-without-cpf.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user with invalid cpf")
    public void trySaveUserWithInvalidCpf() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-cpf-invalid.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user without email")
    public void trySaveUserWithoutEmail() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-without-email.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user with invalid email")
    public void trySaveUserWithInvalidEmail() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-email-invalid.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    @Test
    @DisplayName("Try to save user without birth date")
    public void trySaveUserWithoutBirthDate() throws Exception {
        final String request = Files.readString(
                ResourceUtils.getFile("classpath:com/challenge/project/vaccines/application/user-without-birth-date.json")
                        .toPath()
        );
        new MockError().mockBadRequest(mockMvc,request);
    }

    private void registerUser( final String request ) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( request ));
    }

}

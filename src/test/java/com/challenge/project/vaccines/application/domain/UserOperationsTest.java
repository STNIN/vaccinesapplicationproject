package com.challenge.project.vaccines.application.domain;

import java.util.Optional;

public class UserOperationsTest {

    public static final String EMAIL = "joao@dominio.com";
    public static final String CPF = "614.258.720-18";

    UserOperations newUserOperations( final User userParameter ){
        return new UserOperations() {
            @Override
            public User registerUser(final User user) {
                return user;
            }
            @Override
            public Optional<User> findUserByEmailAOrCpf(final String email, final String cpf) {
                if( email.equals(EMAIL) || cpf.equals(CPF) )
                    return Optional.of( userParameter );
                return Optional.empty();
            }
            @Override
            public Optional<User> findUserByEmail(final String email) {
                if( email.equals(EMAIL) )
                    return Optional.of( userParameter );
                return Optional.empty();
            }
        };
    }

}

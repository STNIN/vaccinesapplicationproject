package com.challenge.project.vaccines.application.domain;

import java.util.Optional;

public interface UserOperations {

    User registerUser( final User user );

    Optional<User> findUserByEmailAOrCpf( final String email, final String cpf );

    Optional<User> findUserByEmail( final String email );

}

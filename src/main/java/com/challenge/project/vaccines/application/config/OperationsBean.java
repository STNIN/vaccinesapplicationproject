package com.challenge.project.vaccines.application.config;

import com.challenge.project.vaccines.application.domain.UserOperations;
import com.challenge.project.vaccines.application.domain.VaccinesApplicationOperations;
import com.challenge.project.vaccines.application.repositories.UserRepository;
import com.challenge.project.vaccines.application.repositories.UserRepositoryOperations;
import com.challenge.project.vaccines.application.repositories.VaccinesApplicationRepository;
import com.challenge.project.vaccines.application.repositories.VaccinesApplicationRepositoryOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OperationsBean {

    @Bean
    public UserOperations userOperations( final UserRepository userRepository ){
        return new UserRepositoryOperations( userRepository );
    }

    @Bean
    public VaccinesApplicationOperations vaccinesApplicationOperations(final VaccinesApplicationRepository vaccinesApplicationRepository ){
        return new VaccinesApplicationRepositoryOperations( vaccinesApplicationRepository );
    }

}

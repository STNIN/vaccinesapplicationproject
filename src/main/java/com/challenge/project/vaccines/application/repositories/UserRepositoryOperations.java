package com.challenge.project.vaccines.application.repositories;

import com.challenge.project.vaccines.application.domain.User;
import com.challenge.project.vaccines.application.domain.UserOperations;

import java.util.Optional;

public class UserRepositoryOperations implements UserOperations {

    private final UserRepository userRepository;

    public UserRepositoryOperations(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User registerUser(final User user) {
        return new UserModel( user ).save( userRepository );
    }

    @Override
    public Optional<User> findUserByEmailAOrCpf(final String email, final String cpf) {
        return userRepository.findFirstUserModelByEmailOrCpf( email, cpf ).map(UserModel::toUser);
    }

    @Override
    public Optional<User> findUserByEmail(final String email) {
        return userRepository.findUserModelByEmail( email ).map(UserModel::toUser);
    }

}

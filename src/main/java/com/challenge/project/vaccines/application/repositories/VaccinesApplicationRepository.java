package com.challenge.project.vaccines.application.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VaccinesApplicationRepository extends JpaRepository<VaccinesApplicationModel, Long> {}

package com.challenge.project.vaccines.application.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserModel, Long> {

    Optional<UserModel> findUserModelByEmail( final String email );

    Optional<UserModel> findFirstUserModelByEmailOrCpf( final String email, final String cpf );

}

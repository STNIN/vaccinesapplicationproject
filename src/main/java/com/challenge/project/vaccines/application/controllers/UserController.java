package com.challenge.project.vaccines.application.controllers;

import com.challenge.project.vaccines.application.application.UserUseCase;
import com.challenge.project.vaccines.application.dto.UserDto;
import com.challenge.project.vaccines.application.dto.Response;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    private final UserUseCase userUseCase;

    public UserController(final UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @PostMapping( "user" )
    @ResponseStatus( HttpStatus.CREATED )
    public @ResponseBody Response saveUser(@RequestBody @Validated final UserDto userDto ){
        return userDto.saveUser( userUseCase );
    }

}

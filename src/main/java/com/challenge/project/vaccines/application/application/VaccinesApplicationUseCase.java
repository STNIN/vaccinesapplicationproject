package com.challenge.project.vaccines.application.application;

import com.challenge.project.vaccines.application.domain.UserOperations;
import com.challenge.project.vaccines.application.domain.VaccinesApplication;
import com.challenge.project.vaccines.application.domain.VaccinesApplicationOperations;

public interface VaccinesApplicationUseCase {

    static VaccinesApplicationUseCase create(final UserOperations userOperations, final VaccinesApplicationOperations vaccinesApplicationOperations){
        return new VaccinesApplicationUseCaseImplementation( userOperations, vaccinesApplicationOperations );
    }

    VaccinesApplication registerVaccinesApplication( final VaccinesApplication vaccinesApplication );

}

package com.challenge.project.vaccines.application.domain;

import java.time.LocalDate;

public class VaccineApplicationMock {

    public VaccinesApplication toVaccineApplication( final String email ){
        return VaccinesApplication.builder()
                .withName("Joao")
                .withUserEmail( email )
                .withApplicationDate( LocalDate.now() )
                .build();
    }

}

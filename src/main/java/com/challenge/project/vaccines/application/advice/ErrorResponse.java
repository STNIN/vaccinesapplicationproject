package com.challenge.project.vaccines.application.advice;

class ErrorResponse {

    private final Integer code;

    private final String message;

    ErrorResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}

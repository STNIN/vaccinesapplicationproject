package com.challenge.project.vaccines.application.domain;

import java.time.LocalDate;

public class VaccinesApplication {

    public static Builder builder(){
        return new Builder();
    }

    private VaccinesApplication( final Builder builder ){
        id = builder.id;
        name = builder.name;
        userEmail = builder.userEmail;
        applicationDate = builder.applicationDate;
    }

    private Long id;
    private final String name;
    private final String userEmail;
    private final LocalDate applicationDate;

    public VaccinesApplication registerVaccinesApplication( final VaccinesApplicationOperations vaccinesApplicationOperations, final UserOperations userOperations ){
        userOperations.findUserByEmail( userEmail )
                .orElseThrow( () -> new UserException("User isn't register !!") );
        return vaccinesApplicationOperations.registerVaccinesApplication( this );
    }

    static public class Builder{

        private Long id;
        private String name;
        private String userEmail;
        private LocalDate applicationDate;

        public Builder withId(final Long id ){
            this.id = id;
            return this;
        }

        public Builder withName( final String name ){
            this.name = name;
            return this;
        }

        public Builder withUserEmail( final String userEmail ){
            this.userEmail = userEmail;
            return this;
        }

        public Builder withApplicationDate( final LocalDate applicationDate ){
            this.applicationDate = applicationDate;
            return this;
        }

        public VaccinesApplication build(){
            return new VaccinesApplication( this );
        }

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public LocalDate getApplicationDate() {
        return applicationDate;
    }

}

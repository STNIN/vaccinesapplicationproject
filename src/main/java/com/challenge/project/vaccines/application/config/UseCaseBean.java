package com.challenge.project.vaccines.application.config;

import com.challenge.project.vaccines.application.application.UserUseCase;
import com.challenge.project.vaccines.application.application.UserUseCaseImplementation;
import com.challenge.project.vaccines.application.application.VaccinesApplicationUseCase;
import com.challenge.project.vaccines.application.application.VaccinesApplicationUseCaseImplementation;
import com.challenge.project.vaccines.application.domain.UserOperations;
import com.challenge.project.vaccines.application.domain.VaccinesApplicationOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseBean {

    @Bean
    public UserUseCase userUseCase( final UserOperations userOperations ){
        return new UserUseCaseImplementation( userOperations );
    }

    @Bean
    public VaccinesApplicationUseCase vaccinesApplicationUseCase( final UserOperations userOperations, final VaccinesApplicationOperations vaccinesApplicationOperations ){
        return new VaccinesApplicationUseCaseImplementation( userOperations, vaccinesApplicationOperations );
    }

}

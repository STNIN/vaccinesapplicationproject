package com.challenge.project.vaccines.application.dto;

import com.challenge.project.vaccines.application.application.UserUseCase;
import com.challenge.project.vaccines.application.domain.User;

import java.time.LocalDate;

public class UserDto {

    private final User.Builder builder;

    public UserDto() {
        builder = User.builder();
    }

    public void setName(final String name) {
        builder.withName( name );
    }

    public void setCpf(final String cpf) {
        builder.withCpf( cpf );
    }

    public void setEmail(final String email) {
        builder.withEmail( email );
    }

    public void setBirthDate(final LocalDate birthDate) {
        builder.withBirthDate( birthDate );
    }

    public Response saveUser(final UserUseCase userUseCase) {
        return new Response(
                userUseCase
                        .registerUser( builder.build() )
                        .getId()
        );
    }

}

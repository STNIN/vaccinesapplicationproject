package com.challenge.project.vaccines.application.dto;

public class Response {

    private final Long id;

    public Response(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}

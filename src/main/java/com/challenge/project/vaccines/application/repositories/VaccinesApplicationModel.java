package com.challenge.project.vaccines.application.repositories;

import com.challenge.project.vaccines.application.domain.VaccinesApplication;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Table( name = "vaccines_application" )
@Entity
class VaccinesApplicationModel {

    private VaccinesApplicationModel(){}

    VaccinesApplicationModel( final VaccinesApplication vaccinesApplication ){
        name = vaccinesApplication.getName();
        userEmail = vaccinesApplication.getUserEmail();
        applicationDate = vaccinesApplication.getApplicationDate();
    }

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotBlank
    @Size( max = 30 )
    @Column(name = "name")
    private String name;

    @NotBlank
    @Email
    @Column(name = "user_email")
    private String userEmail;

    @NotNull
    @Column(name = "application_date")
    private LocalDate applicationDate;

    VaccinesApplication toVaccineApplication(){
        return VaccinesApplication.builder()
                .withId( id )
                .withName( name )
                .withUserEmail( userEmail )
                .withApplicationDate( applicationDate )
                .build();
    }

    VaccinesApplication save(final VaccinesApplicationRepository vaccinesApplicationRepository ) {
        return vaccinesApplicationRepository.save( this ).toVaccineApplication();
    }

}
